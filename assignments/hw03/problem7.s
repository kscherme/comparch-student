    .set noreorder
    .data

    .text
    .globl main
    .ent main
	
print:
        ori $v0, $0, 20
	syscall
	nop
	jr $ra
	nop
	
sum3:
#return a+b+c
	add $t0, $a0, $a1 	#t0 = a0 + a1
	add $v0, $t0, $a2	#s0 = t0 + a2
	jr $ra			# return
	nop
	
polynomial:
        add $sp, $sp, -4        #sp = sp - 8
	sw $ra, 0($sp)          #*(sp+4) = ra
#x = a << b
	sll $a0, $a0, $a1	#a0 = a0 << a1
#y = c << d
	sll $a1, $a2, $a3	#a1 = a2 << a3

	lw $t0, 4($sp)		#s0 = 4(sp)
	add $a2, $0, $t0	#a2 = s0
#z = sum3(x, y, e)
	jal sum3
	nop
	add $t1, $0, $v0	#t1 = v0
#print(x)
	jal print
	nop
#print(y)
	add $a0, $0, $a1	#a0 = a1
	jal print
	nop
#print(z)
	add $a0, $0, $t1	#a0 = s1
	jal print
	nop
#return z
	add $v0, $0, $t1	#v0 = v0

	lw $ra, 0($sp)		#0(sp) = ra
	addi $sp, $sp, 4	#sp = sp +4
	jr $ra
	nop
	
main:
	add $sp, $sp, -8	#sp = sp - 8
	sw $ra, 4($sp)		#*(sp+4) = ra
	
#int a=2
	addi $s0, $0, 2 	#s0 = 2

	addi $a0, $s0, 0	#a0 = s0
	addi $a1, $0, 3		#a1 = 3
	addi $a2, $0, 4		#a2 = 4
	addi $a3, $0, 5		#a3 = 5
	addi $s1, $0, 6		#s1 = 6
	sw $s1, 0($sp)		#*(sp) = s1

#int f = polynomial(a, 3, 4, 5, 6)
	jal polynomial
	nop
	add $s2, $0, $v0	#s2 = v0

#print(a)
	add $a0, $0, $s0	#a0 = s0
	jal print
	nop

#print(f)
	add $a0, $0, $s2	#a0 = s2
	jal print
	nop

	lw $ra, 4($sp)		#4(sp) = ra
	addi $sp, $sp, 8	#sp = sp + 8
        jr $ra
	nop
    .end main
