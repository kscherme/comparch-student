    .set noreorder
    .data
#char A[] = { 1, 25, 7, 9, -1 }
A: .byte 1, 25, 7, 9, -1
	
    .text
    .globl main
    .ent main
	
main:	
#i = 0
	addi $s0, $0, 0         #s0 = 0
#current = A[0]
	lui $t0, %hi(A)
	ori $t0, $t0, %lo(A)
	lb $s1, 0($t0)		#load byte from $t0 into current, s1=1
#max = 0
	addi $s2, $0, 0		#s2 = 0
#while(current > 0)
	slti $t1, $s1, 0
loop_cond:
	slti $t1, $s1, 0	#t1 = !(s1 >= 0) = (s1 < 0)
	bne $t1, $0, sequel2    #if (t1) goto sequel2
	nop
#_if (current > max)
	slt $t2, $s1, $s2   	#t2 = !(s1 >= s2) = (s1 < s2)
	beq $t2, $0, if_state	#if (t2) goto if_state
	nop
	j sequel		#goto sequel
	nop
if_state:
#max = current
	addi $s2, $s1, 0	#s2 = s1
	j sequel		#goto sequel
	nop
sequel:	
#i = i+1
	addi $s0, $s0, 1	#s0 = s0 +1
#current = A[i]
	add $t3, $t0, $s0	
	lb $s1, 0($t3)		#load byte from $t0 + $s0 into current
	j loop_cond		#jump to loop again
	nop
sequel2:
#PRINT_HEX_DEC(max)
	add $a0, $s2, $0
	ori $v0, $0, 20
	syscall

	ori $v0, $0, 10     # exit
	syscall
	

 
        ori $v0, $0, 10     # exit
        syscall
    .end main
