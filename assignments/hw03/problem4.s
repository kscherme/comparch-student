    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
# record *r = (record *) MALLOC(sizeof(record))
	addi $a0, $0, 8		#a0 = 8
	ori $v0, $a0, 9		#make syscall 9
	syscall

	add $s0, $v0, $0	#s0 = v0
# r->field0 = 100
	addi $s1, $0, 100	#s1 = 100
	sw $s1, 0($s0)		#0(s0) = 100
# r->field1 = -1
	addi $s2, $0, -1	#s2 = -1
	sw $s2, 4($s0)		#4(s0) = s2

        ori $v0, $0, 10     # exit
        syscall
    .end main
