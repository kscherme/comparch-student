    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
#int score = 84
	addi $s0, $0, 84	# score = 84
#_if (score >= 90)
	slti $t0, $s0, 90	#t0 = !(s0>= 90) = (s0 < 90)
	bne $t0, $0, else_if	#if (t0) goto else_if
	nop
#grade = 4
	addi $s1, $0, 4		# grade = 4
	j sequel		# goto sequel
	nop

else_if:
#_else if (score >= 80)
	slti $t1, $s0, 80	#t1 = !(s0 >= 80) = (s0 < 80)
	bne $t1, $0, else_if2  	#if (t1) goto else_if2
	nop
#grade = 3
	addi $s1, $0, 3		#int grade = 3
	j sequel		# goto sequel
	nop

else_if2:
#_else if (score >= 70)
	slti $t2, $s0, 70	#t1 = !(s0 >= 70) = (s0 < 70)
	bne $t2, $0, else_block	#if (t2) goto else_block
	nop
#grade = 2
	addi $s1, $0, 2		#grade = 2
	j sequel		#goto sequel
	nop

else_block:
#_else
#grade = 0
	addi $s1, $0, 0		#grade = 0
			
sequel:
#PRINT_HEX_DEC($a0)
	add $a0, $s1, $0	#a0 = s1
	ori $v0, $0, 20		#make syscall 20
	syscall
	
        ori $v0, $0, 10     # exit
        syscall
    .end main
