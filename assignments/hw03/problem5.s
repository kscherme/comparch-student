    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
#elt *head
#elt *newelt
#newelt = (elt *) MALLOC(sizeof(elt))
	addi $a0, $0, 12		#size of the struct is 12
	ori $v0, $0, 9
	syscall

	add $s0, $v0, $0		#s0 = v0
#newelt->value=1
	addi $s1, $0, 1			#s1 = 1
	sw $s1, 0($s0)			#0($s0) = s1
#newelt->next = 0
	addi $s2, $0, 0			#s2 = 0
	sw $s2, 4($s0)			#4($s0) = s2
#head = newelt
	addi $s3, $s0, 0		#s3 = s0

#newelt = (elt *) MALLOC(sizeof(elt))
	addi $a0, $0, 12		#size of the struct is 12
	ori $v0, $0, 9
	syscall

	add $s4, $v0, $0		#s4 = v0
#newelt->value = 2
	addi $t0, $0, 2			#t0 = 2
	sw $t0, 0($s4)			#0(s4) = t0
#newelt->next = head
	sw $s3, 4($s4)			#4(s4) = s3
#head = newelt
	addi $s3, $s4, 0		# s3 = s4

#PRINT_HEX_DEC(head->value)
	lw $s5, 0($s3)			#s5 = 0(s3)
	add $a0, $s5, $0
	ori $v0, $0, 20
	syscall

#PRINT_HEX_DEC(head->next->value)
	lw $s6, 4($s3)			#s6 = 4(s3)
	lw $s7, 0($s6)			#s7 = 0(s6)
	add $a0, $s7, $0		#a0 = s7
	ori $v0, $0, 20
	syscall

        ori $v0, $0, 10     # exit
        syscall
    .end main
