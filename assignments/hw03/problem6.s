    .set noreorder
    .data

    .text
    .globl main
    .ent main

print:
	ori $v0, $0, 20
	syscall
	jr $ra
	nop
	
main:
	addi $s0, $0, 0 	#s0 = 0
	addi $s1,$0, 1		#s1 = 1

	add $sp, $sp, -36	#sp = sp - 36
	sw $ra, 32($sp)		#*(sp+32) = ra
	
#A[0] = 0
	add $t0, $0, $sp	#t0 = A
        sw $s0,0($t0)           #0(A) = s0
# A[1] = 1
        sw $s1,4($t0)           #4(A) = s1
#for (i=2,i<8,i++)
        addi $s2, $0, 2         #s2 = 2
loop_cond:
        slti $t1, $s2, 8        #t1 = !(s2 >=8) = (s2 < 8)
        beq $t1, $0, sequel     #if (t1) goto sequel
        nop
#A[i] = A[i-1] + A[i-2]
        sll $t2, $s2, 2         #t2 = i*4
        addi $t3, $0, 8
        sub $t4, $t2, $t3       #t4 = t2 - 8
        add $t4, $t0, $t4       #t4 = A + t4
        lw $s3, 0($t4)          #s3 = A[i-2]

	addi $t5, $0, 4
        sub $t6, $t2, $t5       #t6 = t2 - 4
        add $t6, $t0, $t6       #t6 = A + t6
        lw $s4, 0($t6)          #s4 = A[i-1]

	add $s5, $s4, $s3       #s5 = s4 + s3
        add $s7, $t2, $t0
	sw $s5, 0($s7)          #A[i] = s5

#print(A[i])
        lw $a0, 0($s7)		#a0 = 0($s7)
        jal print
	nop
	
        addi $s2, $s2, 1	#s2 = s2 + 1
        j loop_cond
        nop
sequel:
        lw $ra, 32($sp)		#ra = *(sp+32)     # exit
	addi $sp, $sp, 36	#sp = sp+36
	jr $ra
	nop

    .end main
